/**
 * Created by jake on 7/4/14.
 */
public class Carrier extends Ship implements Damage{

	private final int size = 5;
	private int health = size;
	public final String name = "Carrier";

	Carrier () {
		super.size = size;
		super.name = name;
	}

	public boolean isSunk () {
		if (health < 1){
			return true;
		}
		else {
			return false;
		}
	}

	public void hit (){
		health--;
	}

	public int getHealth() {
		return health;
	}
}
