/**
 * Created by jake on 7/4/14.
 */
public class AttackBoat extends Ship implements Damage{

	private final int size = 3;
	private int health = size;
	public final String name = "Attack Boat";

	AttackBoat () {
		super.size = size;
		super.name = name;
	}

	public boolean isSunk () {
		if (health < 1){
			return true;
		}
		else {
			return false;
		}
	}

	public void hit (){
		health--;
	}

	public int getHealth() {
		return health;
	}
}
