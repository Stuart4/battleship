/**
 * Created by jake on 7/4/14.
 */
import java.util.Random;
public class Board {

	final int xDim = 7, yDim = 7;
	private int[][] board = new int[xDim][yDim];
	private Ship[][] shipsAO = new Ship[xDim][yDim];
	private int playerMoves = 0;
	private Ship[] ships;
	private int shipsRemaining;

	Board (){
		Random rnd = new Random();
//		System.out.println(rnd.nextInt(8));

		ships = new Ship[2];
		ships[0] = new AttackBoat();
		ships[1] = new Carrier();
		shipsRemaining = ships.length;



		for (Ship k : ships){
			 while (!placeRnd(k));
		}

		System.out.println(this.toString());
	}

	private boolean placeRnd(Ship ship){
		Random rnd = new Random();

		if (rnd.nextBoolean()) {
			int index = rnd.nextInt(7);
			int anInt = rnd.nextInt(7);
			if (doesFit(ship, index, board[anInt])) {
				placeShip(ship, true, index, anInt);
				return true;
			} else {
				return false;
			}
		} else {
			int index = rnd.nextInt(7);
			int columnIndex = rnd.nextInt(7);
			if (doesStand(ship, index, columnIndex)){
				placeShip(ship, false, index, columnIndex);
				return true;
			} else {
				return false;
			}
			}
	}

	private void placeShip(Ship ship, boolean horizontal, int startX, int startY){
		if (horizontal){
			for (int i = 0; i < ship.size; i++){
				board[startY][startX + i] = 1;
				shipsAO[startY][startX + i] = ship;
			}
		} else {
			for (int i = 0; i < ship.size; i++){
				board[startY + i][startX] = 1;
				shipsAO[startY + i][startX] = ship;
			}
		}
	}
	private boolean doesFit(Ship ship, int index, int[] spaces){
		int length = ship.getSize();

		if ( (length + index) > spaces.length - 1){
			return false;
		}

		for (int i = index; i < spaces.length && i < length + index; i++){
			if (spaces[i] == 1){
				return false;
			}
		}

		return true;
	}

	private boolean doesStand (Ship ship, int index, int columnIndex){

		if (columnIndex + ship.size > board[0].length){
			return false;
		}

		int[] horizontal = new int[yDim];

		for (int i = 0; i < horizontal.length; i++){
			horizontal[i] = board[i][columnIndex];
		}

		return doesFit(ship, index, horizontal);
	}

	public String toString(){
		String out = " ";

		for (int i = 0; i < board[0].length; i++){
			out += " " + Integer.toString(i + 1) + " ";
		}
		out += "\nA";

		char letter = 'A';

		for (int i = 0; i < board[0].length; i++){
			for (int n = 0; n < board.length; n++){
				switch (board[i][n]){
					case (0):
//						out += " % ";     //Enable to debug
//						break;
					case (1):
						out += " = ";
						break;
					case (2):
						out += " 0 ";
						break;
					case (3):
						out += " * ";
						break;
					}
				}
			letter++;
			out += "\n" + letter;
			}
		out = out.substring(0, out.lastIndexOf("\n"));
		return out;
		}

public void fire (char[] coordinates){
	int y = coordinates[1] -'1';
	int x = coordinates[0] - 'A';

	playerMoves++;

	switch (board[x][y]){
		case (0):
			board[x][y] = 2;
			break;
		case(1):
			board[x][y] = 3;
			shipsAO[x][y].hit();
			if (shipsAO[x][y].isSunk()){
				shipsRemaining--;
				System.out.printf("You sunk a %s\n", shipsAO[x][y].name);
				System.out.printf("%d ship(s) remain(s) of %d.\n",shipsRemaining, ships.length );
				if (shipsRemaining < 1){
					System.out.println("You Win! Hit enter to exit and view your stats.");
				}
			} else {
				System.out.printf("You hit a %s\n", shipsAO[x][y].name);
			}
			break;
		case (3):
		case (4):
			playerMoves--;
			System.out.println("You already played there.");
			break;
	}
	}

	private boolean isSunk(Ship ship, int x, int y) {
		return ((Damage)ship).isSunk();
	}

	public void setShipsAO(Ship[][] shipsAO) {
		this.shipsAO = shipsAO;
	}

	public int getPlayerMoves(){
		return playerMoves;
	}

	public int getShipCount(){
		return ships.length;
	}

	public int getShipsRemaining(){
		return shipsRemaining;
	}
}

