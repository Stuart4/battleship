/**
 * Created by jake on 7/4/14.
 */
interface Damage {

	boolean isSunk ();
	void hit ();
	int getHealth ();
}
