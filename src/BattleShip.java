/**
 * Created by jake on 7/4/14.
 */
import java.util.Scanner;
public class BattleShip {

	public static void main(String[] args){
		System.out.println("Welcome to battleship!");
		Board board = new Board();
		Scanner sin = new Scanner(System.in);
		BattleShip bs = new BattleShip();
		char[] coordinates = new char[2];


		String in = "hotdog";
		while (!in.equals("")){

			if(in.length() == 2 && bs.validateCoordinates(coordinates, board)){
				coordinates[0] = in.charAt(0);
				coordinates[1] = in.charAt(1);
				board.fire(coordinates);
				System.out.println(board.toString());
			}
			System.out.printf("Enter coordinates (D4):");

			in = sin.nextLine().toUpperCase();
			in = in.replaceAll("[^A-Z0-9]", "");
		}
		System.out.printf("You played %d moves to eliminate %d of %d ships.", board.getPlayerMoves(),board.getShipCount() - board.getShipsRemaining(), board.getShipCount());

	}

	private boolean validateCoordinates(char[] coordinates, Board board){

		if(coordinates.length < 2){ return false; }

		if (coordinates.length > 2){
			char[] tmp = coordinates;
			coordinates = new char[1];
			coordinates[0] = tmp[0];
			coordinates[1] = tmp[1];
		}

		if (coordinates[1] > '0' + board.xDim){ return false; }

		if (coordinates[0] > 'a' + board.yDim - 1){ return false; }

		return true;
	}

}
